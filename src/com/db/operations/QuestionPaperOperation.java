/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewQuestionPaperBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Aniket
 */
public class QuestionPaperOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<QuestionBean> getPreviousYearPaperQuestionList() {
        ArrayList<QuestionBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM QUESTION_PAPER_INFO";
            ps=conn.prepareStatement(query);
            rs=ps.executeQuery();
            QuestionBean bean = null;
            
            while (rs.next()) {
                bean=new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();
                
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<QuestionBean> getPreviousYearWiseQuestionList(ArrayList<String> yearList) {
        ArrayList<QuestionBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
//            String query = "SELECT * FROM QUESTION_PAPER_INFO  WHERE QUESTION_YEAR = ? AND (ISOPTIONASIMAGE = true OR ISQUESTIONASIMAGE = true OR ISHINTASIMAGE = true) ORDER BY QUESTION_YEAR,SUBJECT_ID";
            String query = "SELECT * FROM QUESTION_PAPER_INFO  WHERE QUESTION_YEAR = ? ORDER BY QUESTION_YEAR,SUBJECT_ID";
            ps = conn.prepareStatement(query);
            for(String year : yearList) {
                ps.setString(1, year);
                rs = ps.executeQuery();
                
                QuestionBean bean = null;
                while (rs.next()) {
                    bean=new QuestionBean();
                    bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                    bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                    bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                    bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                    bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                    bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                    bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                    bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                    bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                    bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                    bean.setYear(rs.getString(21));                  bean.setSelected(false);
                
                    if(returnList == null)
                        returnList = new ArrayList<QuestionBean>();

                    returnList.add(bean);
                }
            }
            
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<String> getYearList() {
        ArrayList<String> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT DISTINCT QUESTION_YEAR FROM QUESTION_PAPER_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            String year = null;
            while(rs.next()) {
                
                year = rs.getString(1);
                
                if(returnList == null)
                    returnList = new ArrayList<String>();
                
                returnList.add(year.trim());
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        
        if(returnList != null)
            Collections.sort(returnList);
        return returnList;
    }
    
    
    public ArrayList<ViewQuestionPaperBean> getViewQuestionPaperList() { 
        ArrayList<ViewQuestionPaperBean> returnList = null;
        ArrayList<QuestionBean> questionList = getPreviousYearPaperQuestionList();
        ArrayList<String> yearList = getYearList();
        ArrayList<SubjectBean> subjectList = new SubjectOperation().getSubjectsList();
        int firstSubId = 0;
        int secondSubId = 0;
        int thirdSubId = 0;
        int index = 0;
        for(SubjectBean sb : subjectList) {
            if(index == 0)
                firstSubId = sb.getSubjectId();
            else if(index == 1)
                secondSubId = sb.getSubjectId();
            else if(index == 2)
                thirdSubId = sb.getSubjectId();
            index++;
        }
        ViewQuestionPaperBean viewQuestionPaperBean = null;
        if(yearList != null) {
            for(String year : yearList) {
                viewQuestionPaperBean = new ViewQuestionPaperBean();
                int totalQues = 0;
                int subFirstQues = 0;
                int subSecondQues = 0;
                int subThirdQues = 0;
                int usedQues = 0;
                for(QuestionBean questionsBean : questionList) {
                    if(year.trim().equalsIgnoreCase(questionsBean.getYear().trim())) {
                        if(firstSubId == questionsBean.getSubjectId())
                            subFirstQues += 1;
                        else if(secondSubId == questionsBean.getSubjectId())
                            subSecondQues += 1;
                        else if(thirdSubId == questionsBean.getSubjectId())
                            subThirdQues += 1;
                        
                        if(questionsBean.getAttempt() == 1) {
                            usedQues += 1;
                        } 
                        
                        totalQues += 1;
                    }
                }
                viewQuestionPaperBean.setPreviousYear(year);
                viewQuestionPaperBean.setTotalQues(totalQues);
                viewQuestionPaperBean.setSubjectFirstQues(subFirstQues);
                viewQuestionPaperBean.setSubjectSecondQues(subSecondQues);
                viewQuestionPaperBean.setSubjectThirdQues(subThirdQues);
                viewQuestionPaperBean.setUsedQues(usedQues);
                
                if(returnList == null)
                    returnList = new ArrayList<ViewQuestionPaperBean>();
                
                returnList.add(viewQuestionPaperBean);
            }
        }
        return returnList;
    }
    
    public ArrayList<String> getPreviousPaperYearList() {
        ArrayList<String> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT QUESTION_YEAR FROM QUESTION_PAPER_INFO GROUP BY QUESTION_YEAR ORDER BY QUESTION_YEAR";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                if(returnList == null)
                    returnList = new ArrayList<String>();
                
                returnList.add(rs.getString(1));
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        
        return returnList;
    }
    
    public boolean updateModifyQuestion(QuestionBean questionBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try{
            String query = "UPDATE QUESTION_PAPER_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,"
                    + "ANSWER = ?,HINT = ?,QUESTION_LEVEL = ?,CHAPTER_ID = ?,TOPIC_ID = ?,ISQUESTIONASIMAGE = ?,"
                    + "QUESTIONIMAGEPATH = ?,ISOPTIONASIMAGE = ?,OPTIONIMAGEPATH = ?,ISHINTASIMAGE = ?,HINTIMAGEPATH = ?,"
                    + "QUESTION_TYPE = ?,QUESTION_YEAR = ? WHERE QUESTION_ID = ?";
            ps=conn.prepareStatement(query);
            ps.setString(1, questionBean.getQuestion());
            ps.setString(2, questionBean.getOptionA());
            ps.setString(3, questionBean.getOptionB());
            ps.setString(4, questionBean.getOptionC());
            ps.setString(5, questionBean.getOptionD());
            ps.setString(6, questionBean.getAnswer());
            ps.setString(7, questionBean.getHint());
            ps.setInt(8, questionBean.getLevel());
            ps.setInt(9, questionBean.getChapterId());
            ps.setInt(10, questionBean.getTopicId());
            ps.setBoolean(11, questionBean.isIsQuestionAsImage());
            ps.setString(12, questionBean.getQuestionImagePath());
            ps.setBoolean(13, questionBean.isIsOptionAsImage());
            ps.setString(14, questionBean.getOptionImagePath());
            ps.setBoolean(15, questionBean.isIsHintAsImage());
            ps.setString(16, questionBean.getHintImagePath());
            ps.setInt(17, questionBean.getType());
            ps.setString(18, questionBean.getYear());
            ps.setInt(19, questionBean.getQuestionId());
            ps.executeUpdate();
            returnValue = true;
        }
        catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public void updateUsedValue(ArrayList<QuestionBean> questionList) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE QUESTION_PAPER_INFO SET ATTEMPTEDVALUE = ? WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, questionBean.getAttempt());
                ps.setInt(2, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public void setInitialUsedValue() {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE QUESTION_PAPER_INFO SET ATTEMPTEDVALUE=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, 0);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
