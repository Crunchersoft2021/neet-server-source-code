/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.QuestionBean;
import com.db.DbConnection;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aniket
 */
public class SaveTestBlobOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    
    public ArrayList<QuestionBean> getTestDetails(int rollNo, int testBeanId) {
        ArrayList<QuestionBean> ret = new ArrayList<QuestionBean>();
        conn = new DbConnection().getConnection();
        try {
            String sql = "select * from StudentTestBean where TestBeanId=? and rollno=?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, testBeanId);
            ps.setInt(2, rollNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                byte[] data = rs.getBytes(3);
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                ret = (ArrayList<QuestionBean>) obj;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SaveTestBlobOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }
}
