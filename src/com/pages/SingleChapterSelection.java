/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pages;

import com.bean.ChapterBean;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import com.bean.ViewChapterBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import com.db.operations.QuestionOperation;
import com.Model.TitleInfo;

/**
 *
 * @author admin
 */
public class SingleChapterSelection extends javax.swing.JFrame {

    /** Creates new form SingleChapterSelection */
    
    private javax.swing.JButton[] chapterSelectionButtons;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<ViewChapterBean> viewChaptersList;
    private SubjectBean selectedSubjectBean;
    private ChapterBean selectedChapterBean;
    private String printingPaperType;

    //Select ChapterWise
    public SingleChapterSelection(SubjectBean selectedSubjectBean,String printingPaperType) {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.getContentPane().setBackground(new Color(0,102,102));
        setExtendedState(MAXIMIZED_BOTH);
        this.printingPaperType = printingPaperType;
        this.selectedSubjectBean = selectedSubjectBean;
        viewChaptersList = new ChapterOperation().getChaptersViewList(selectedSubjectBean.getSubjectId());
       // System.out.println("selectedSubjectBean.getSubjectId()"+selectedSubjectBean.getSubjectId());
        setChapterPanel();
    }

    private void setChapterPanel() {
        chapterSelectionButtons = new JButton[viewChaptersList.size()];
        System.out.println("viewChaptersList.size()="+viewChaptersList.size());
        PanelBody.removeAll();
        LblSubject.setText(selectedSubjectBean.getSubjectName());
        System.out.println("selectedSubjectBean.getSubjectName()=" +selectedSubjectBean.getSubjectName());
        for (int x = 0; x < viewChaptersList.size(); x++) { 
            chapterSelectionButtons[x] = new JButton();            
            chapterSelectionButtons[x].setText("<html>"
                                        + "<b><font color=#EF9433>" + viewChaptersList.get(x).getChapterBean().getChapterName()
                                        + "</font></b><hr>Total Questions: " + viewChaptersList.get(x).getTotalQue()
                                        + "<br />Easy: " + viewChaptersList.get(x).getEasy() 
                                        + "<br />Medium: " + viewChaptersList.get(x).getMedium()
                                        + "<br />Hard: " + viewChaptersList.get(x).getHard()
                                        + "<br />Numerical: " + viewChaptersList.get(x).getNumerical()
                                        + "<br />Theoretical: " + viewChaptersList.get(x).getTheory()
                                        + "<br />Used: " + viewChaptersList.get(x).getUsed() 
                                        + "<br />Previously Asked: " + viewChaptersList.get(x).getAsked()
                                        + "<br />Available Hints: " + viewChaptersList.get(x).getHint()
                                        + "</html>");
            
            chapterSelectionButtons[x].setMaximumSize(new java.awt.Dimension(200, 220));
            chapterSelectionButtons[x].setMinimumSize(new java.awt.Dimension(200, 220));
            chapterSelectionButtons[x].setPreferredSize(new java.awt.Dimension(200, 220));
            chapterSelectionButtons[x].setBorderPainted(false);
            chapterSelectionButtons[x].setBackground(new java.awt.Color(11, 45, 55));
            chapterSelectionButtons[x].setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12));
            chapterSelectionButtons[x].setForeground(new java.awt.Color(255, 255, 255));
            chapterSelectionButtons[x].addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    setButtonsAction(e.getActionCommand());
                }
            });
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(3, 3, 3, 3);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < viewChaptersList.size(); x++) {
            if (x % 6 == 0) {
                cons.gridy++;
                cons.gridx = 0;
            }
            PanelBody.setLayout(layout);
            PanelBody.add(chapterSelectionButtons[x], cons);
            cons.gridx++;
        }   
        PanelBody.validate();
        PanelBody.repaint();
    }

    public void setButtonsAction(String actionCommand) {
        try {
           // System.out.println("actionCommand"+actionCommand);
            ArrayList<QuestionBean> sortQuestionsList=new ArrayList<QuestionBean>();
            String strSplt[] = actionCommand.split("EF9433>");            
            //System.out.println("strSplt[0]="+strSplt[0]);    
           // System.out.println("strSplt[1]="+strSplt[1]);
            
            String chapterName = strSplt[1];           
            strSplt = chapterName.split("<");           
           // System.out.println("strSplt="+strSplt);
            chapterName = strSplt[0];
            
        //System.out.println("chapterName="+chapterName);
            
        for (int i = 0; i < viewChaptersList.size(); i++) {
                if (viewChaptersList.get(i).getChapterBean().getChapterName().equals(chapterName)) {
                    selectedChapterBean = viewChaptersList.get(i).getChapterBean();
                    break;
                }
            }
            
            String strOption = "All";
            int resultOption=0;
            Object[] possibilities = {"All", "With Hints", "Without Hints"};
            strOption = (String) JOptionPane.showInputDialog(null, "Select Question Type:\n", "", JOptionPane.PLAIN_MESSAGE, null, possibilities, "ham");
           
           // System.out.println("strOption="+strOption);
            
            if(strOption != null) {
                if (strOption.endsWith("All")) {
                    resultOption=0;
                } else if (strOption.endsWith("With Hints")) {
                    resultOption=1;
                } else if (strOption.endsWith("Without Hints")) {
                    resultOption=2;
                }

                questionsList = new QuestionOperation().getQuestuionsChapterWise(selectedChapterBean.getChapterId());

                if(resultOption==0) {
                    sortQuestionsList = questionsList;
                } else {
                    for(QuestionBean questionsBean : questionsList) {
                        if(resultOption==1) {
                            if(!questionsBean.getHint().equals("") && !questionsBean.getHint().equals(" ") && !questionsBean.getHint().equals(null) && !questionsBean.getHint().equals("\\mbox{}") && !questionsBean.getHint().equals("\\mbox{ }"))
                                sortQuestionsList.add(questionsBean);   
                        } else if(resultOption==2) {
                            if(questionsBean.getHint().equals("") || questionsBean.getHint().equals(" ") || questionsBean.getHint().equals(null) || questionsBean.getHint().equals("\\mbox{}") || questionsBean.getHint().equals("\\mbox{ }"))
                                sortQuestionsList.add(questionsBean);
                        }   
                    }
                }
                new SingleChapterQuestionsSelection(selectedChapterBean,selectedSubjectBean,sortQuestionsList,printingPaperType).setVisible(true);
                this.dispose();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BodyScrollPane = new javax.swing.JScrollPane();
        PanelBody = new javax.swing.JPanel();
        PanelHeader = new javax.swing.JPanel();
        LblSubject = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        LblBack = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        BodyScrollPane.setBorder(null);
        BodyScrollPane.setName("BodyScrollPane"); // NOI18N

        PanelBody.setBackground(new java.awt.Color(0, 102, 102));
        PanelBody.setForeground(new java.awt.Color(255, 255, 255));
        PanelBody.setName("PanelBody"); // NOI18N

        javax.swing.GroupLayout PanelBodyLayout = new javax.swing.GroupLayout(PanelBody);
        PanelBody.setLayout(PanelBodyLayout);
        PanelBodyLayout.setHorizontalGroup(
            PanelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        PanelBodyLayout.setVerticalGroup(
            PanelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 263, Short.MAX_VALUE)
        );

        BodyScrollPane.setViewportView(PanelBody);

        PanelHeader.setBackground(new java.awt.Color(0, 102, 102));
        PanelHeader.setName("PanelHeader"); // NOI18N

        LblSubject.setBackground(new java.awt.Color(0, 102, 102));
        LblSubject.setFont(new java.awt.Font("Microsoft JhengHei", 1, 24)); // NOI18N
        LblSubject.setForeground(new java.awt.Color(255, 255, 255));
        LblSubject.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSubject.setText("Subject");
        LblSubject.setName("LblSubject"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));
        jPanel2.setName("jPanel2"); // NOI18N

        LblBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg2.png"))); // NOI18N
        LblBack.setName("LblBack"); // NOI18N
        LblBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblBackMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(LblBack, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(LblBack)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout PanelHeaderLayout = new javax.swing.GroupLayout(PanelHeader);
        PanelHeader.setLayout(PanelHeaderLayout);
        PanelHeaderLayout.setHorizontalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelHeaderLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSubject, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        PanelHeaderLayout.setVerticalGroup(
            PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelHeaderLayout.createSequentialGroup()
                .addGroup(PanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblSubject, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BodyScrollPane)
            .addComponent(PanelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        new HomePage().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void LblBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseExited
        // TODO add your handling code here:
        LblBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg2.png")));
    }//GEN-LAST:event_LblBackMouseExited

    private void LblBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseEntered
        // TODO add your handling code here:
        LblBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/BackImg1.png")));
    }//GEN-LAST:event_LblBackMouseEntered

    private void LblBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackMouseClicked
        // TODO add your handling code here:
//        new HomePage(1).setVisible(true);
        new HomePage(printingPaperType).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_LblBackMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(SingleChapterSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new SingleChapterSelection(2,1).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane BodyScrollPane;
    private javax.swing.JLabel LblBack;
    private javax.swing.JLabel LblSubject;
    private javax.swing.JPanel PanelBody;
    private javax.swing.JPanel PanelHeader;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}