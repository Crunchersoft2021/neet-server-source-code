/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing;

import com.LatexProcessing.word.NewQuesLatexProcessing;
import com.bean.QuestionBean;

/**
 *
 * @author Aniket
 */
public class ErrorManager {
    
    public String getErrorString(QuestionBean questionsBean,boolean isIsOptionAsImage) {
        String returnString = "";
        int dollarCount,slashCount;
        dollarCount = getDolloarCount(questionsBean.getQuestion());
        slashCount = isSlashCorrect(questionsBean.getQuestion());
        if(dollarCount % 2 != 0 && slashCount == 0)
            returnString += "Question : Error in $ & \\\n";
        else if(dollarCount % 2 != 0)
            returnString += "Question : Error in $\n";
        else if(slashCount == 0)
            returnString += "Question : Error in \\\n";
        
        if(!isIsOptionAsImage) {
            dollarCount = getDolloarCount(questionsBean.getOptionA());
            slashCount = isSlashCorrect(questionsBean.getOptionA());
            if(dollarCount % 2 != 0 && slashCount == 0)
                returnString += "Option A : Error in $ & \\\n";
            else if(dollarCount % 2 != 0)
                returnString += "Option A : Error in $\n";
            else if(slashCount == 0)
                returnString += "Option A : Error in \\\n";
            
            dollarCount = getDolloarCount(questionsBean.getOptionB());
            slashCount = isSlashCorrect(questionsBean.getOptionB());
            if(dollarCount % 2 != 0 && slashCount == 0)
                returnString += "Option B : Error in $ & \\\n";
            else if(dollarCount % 2 != 0)
                returnString += "Option B : Error in $\n";
            else if(slashCount == 0)
                returnString += "Option B : Error in \\\n";
            
            dollarCount = getDolloarCount(questionsBean.getOptionC());
            slashCount = isSlashCorrect(questionsBean.getOptionC());
            if(dollarCount % 2 != 0 && slashCount == 0)
                returnString += "Option C : Error in $ & \\\n";
            else if(dollarCount % 2 != 0)
                returnString += "Option C : Error in $\n";
            else if(slashCount == 0)
                returnString += "Option C : Error in \\\n";
            
            dollarCount = getDolloarCount(questionsBean.getOptionD());
            slashCount = isSlashCorrect(questionsBean.getOptionD());
            if(dollarCount % 2 != 0 && slashCount == 0)
                returnString += "Option D : Error in $ & \\\n";
            else if(dollarCount % 2 != 0)
                returnString += "Option D : Error in $\n";
            else if(slashCount == 0)
                returnString += "Option D : Error in \\\n";
        }
        
        dollarCount = getDolloarCount(questionsBean.getHint());
        slashCount = isSlashCorrect(questionsBean.getHint());
        if(dollarCount % 2 != 0 && slashCount == 0)
            returnString += "Hint : Error in $ & \\\n";
        else if(dollarCount % 2 != 0)
            returnString += "Hint : Error in $\n";
        else if(slashCount == 0)
            returnString += "Hint D : Error in \\\n";
            
        if(returnString.equalsIgnoreCase(""))
            returnString = null;
        else
            returnString = returnString.substring(0,returnString.length()-1);
        return returnString;
    }
    
    private int getDolloarCount(String str) {
        int dollarcount = 0;
        char[] carray = str.toCharArray();
        int clen = carray.length;
        for (int i = 0; i < clen;i++) {
            if (carray[i] == '$') 
                dollarcount++;
        }
        return dollarcount;
    }
    
    private int isSlashCorrect(String str) {
        str = new NewQuesLatexProcessing().mboxProcessing(str);
        int dollarcount = 0;
        char[] carray = str.toCharArray();
        int clen = carray.length;
        for (int i = 0; i < clen;) {

            if (carray[i] == '\\' && (dollarcount % 2 == 0)) {
                if(iscmd(str, i)==0)
                {
                    return 0;
                }              
                
            }

            if (carray[i] == '$') {

                dollarcount++;
                i++;
            } else {
                i++;
            }
        }
        return 1;
    }
    
    public int iscmd(String str, int index) {
        char[] carray = str.toCharArray();
        int clen = carray.length;

        if ((clen > (index + 6)) 
                && ((carray[index] == '\\' && carray[index + 1] == 't' && carray[index + 2] == 'e' && carray[index + 3] == 'x' && carray[index + 4] == 't' && carray[index + 5] == 'i' && carray[index + 6] == 't')
                || (carray[index] == '\\' && carray[index + 1] == 't' && carray[index + 2] == 'e' && carray[index + 3] == 'x' && carray[index + 4] == 't' && carray[index + 5] == 'b' && carray[index + 6] == 'f')
                || (carray[index] == '\\' && carray[index + 1] == 'm' && carray[index + 2] == 'i' && carray[index + 3] == 'n' && carray[index + 4] == 'u' && carray[index + 5] == 's')
                || (carray[index] == '\\' && carray[index + 1] == ' ' ))) {
            return 1;
        } else {
            return 0;
        }
    }
}
