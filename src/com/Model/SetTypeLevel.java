/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.ChapterBean;
import com.bean.LevelBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubjectOperation;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Aniket
 */
public class SetTypeLevel {
    private ArrayList<SubjectBean> subjectList;
    private ArrayList<QuestionBean> allQuestionList;
    private ArrayList<ChapterBean> allChapterList;
    
    public SetTypeLevel() {
        subjectList = new SubjectOperation().getSubjectsList();
        allQuestionList = new QuestionOperation().getQuestuionsList();
        allChapterList = new ChapterOperation().getChapterList();
    }
    
    public void dataRefining() {
//        int index = 0;
        if(subjectList != null && allQuestionList != null && allChapterList != null) {
            for(SubjectBean subjectBean : subjectList) {
//                index++;
                ArrayList<ChapterBean> selectedChapterList = getChapterList(subjectBean);
                if(selectedChapterList != null) {
                    for(ChapterBean chapterBean : selectedChapterList) {
                        ArrayList<LevelBean> levelList = null;
                        LevelBean levelBean = null;
                        ArrayList<QuestionBean> seleQuestionsList = getQuestionsList(chapterBean);
                        if(seleQuestionsList != null) {
                            if(levelList == null)
                                levelList = new ArrayList<LevelBean>();
                            else
                                levelList.clear();
                            int listIndex = 0;
                            String question = "";
                            for(QuestionBean questionBean : seleQuestionsList) {
                                levelBean = new LevelBean();
                                levelBean.setQuestionId(questionBean.getQuestionId());
                                levelBean.setHintSize(questionBean.getHint().length());
                                levelBean.setQuestionIndex(listIndex++);
                                levelList.add(levelBean);
                                if(subjectBean.getSubjectId() == 3) {
                                    questionBean.setType(1);
                                } else if(subjectBean.getSubjectId() == 4) {
                                    questionBean.setType(0);
                                } else {
                                    question = questionBean.getQuestion().trim();
                                    if(question.contains("0") || question.contains("1") || question.contains("2") || 
                                        question.contains("3") || question.contains("4") || question.contains("5") || 
                                        question.contains("6") || question.contains("7") || question.contains("8") ||  
                                        question.contains("9")) {
                                        questionBean.setType(1);
                                    } else {
                                        questionBean.setType(0);
                                    }
                                }
                            }
                            
                            if(levelList.size() != 0) {
                                Collections.sort(levelList);
                                int totalQues = levelList.size();
                                
                                int easyCount = (int) (totalQues * 0.40);
                                int mediumCount = (int) (totalQues * 0.40);
                                int hardCount = (int) (totalQues * 0.20);
                                
                                if(easyCount + mediumCount + hardCount != totalQues) 
                                    easyCount += totalQues - (easyCount + mediumCount + hardCount);
                                
                                for(int i=0;i<levelList.size();i++) {
                                    if(i<easyCount)
                                        seleQuestionsList.get(levelList.get(i).getQuestionIndex()).setLevel(0);
                                    else if(i< (mediumCount * 2))
                                        seleQuestionsList.get(levelList.get(i).getQuestionIndex()).setLevel(1);
                                    else
                                        seleQuestionsList.get(levelList.get(i).getQuestionIndex()).setLevel(2);
                                }
                                
//                                for(QuestionsBean questionBean : seleQuestionsList) {
//                                    System.out.println("QuesId : "+questionBean.getQuestionId()+"\tLevel : "+questionBean.getLevel()+"\tType:"+questionBean.getType());
//                                }
                                new QuestionOperation().updateRefinedValue(seleQuestionsList);
                            }
                        }
                    }
                }
//                if(index == 1)
//                    break;
            }
        }
    }
    
    private ArrayList<ChapterBean> getChapterList(SubjectBean subjectBean) {
        ArrayList<ChapterBean> returnList = null;
        if(allChapterList != null) {
            for(ChapterBean chapterBean : allChapterList) {
                if(chapterBean.getSubjectId() == subjectBean.getSubjectId()) {
                    if(returnList == null)
                        returnList = new ArrayList<ChapterBean>();
                    returnList.add(chapterBean);
                }
            }
        }
        return returnList;
    }
    
    private ArrayList<QuestionBean> getQuestionsList(ChapterBean chapterBean) {
        ArrayList<QuestionBean> returnList = null;
        if(allQuestionList != null) {
            for(QuestionBean questionBean : allQuestionList) {
                if(questionBean.getChapterId() == chapterBean.getChapterId()) {
                    if(returnList == null)
                        returnList = new ArrayList<QuestionBean>();
                    returnList.add(questionBean);
                }
            }
        }
        return returnList;
    }
    
    public static void main(String[] args) {
        new SetTypeLevel().dataRefining();
    }
}
