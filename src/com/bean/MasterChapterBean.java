/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class MasterChapterBean {
    private int chapterId;
    private String chapterName;
    private MasterSubjectBean subjectBean;
    private MasterBookBean bookBean;

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public MasterSubjectBean getSubjectBean() {
        return subjectBean;
    }

    public void setSubjectBean(MasterSubjectBean subjectBean) {
        this.subjectBean = subjectBean;
    }

    public MasterBookBean getBookBean() {
        return bookBean;
    }

    public void setBookBean(MasterBookBean bookBean) {
        this.bookBean = bookBean;
    }
}
