/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Registration;

import java.security.NoSuchAlgorithmException;
/**
 *
 * @author admin
 */
public class HashCodeGenerator {
    
    public String pinHash(String key, String algorithmName) {
        String hexMessageEncode = "";
        try {
            byte[] buffer = key.getBytes();
            java.security.MessageDigest messageDigest =
                    java.security.MessageDigest.getInstance(algorithmName);
            messageDigest.update(buffer);
            byte[] messageDigestBytes = messageDigest.digest();
            for (int index = 0; index < messageDigestBytes.length; index++) {
                int countEncode = messageDigestBytes[index] & 0xff;
                if (Integer.toHexString(countEncode).length() == 1) {
                    hexMessageEncode = hexMessageEncode + "0";
                }
                hexMessageEncode = hexMessageEncode + Integer.toHexString(countEncode);
            }
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return hexMessageEncode;
    }
}
